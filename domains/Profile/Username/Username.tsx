import { Box } from '@chakra-ui/layout'
import { Button, Input } from '@chakra-ui/react'
import type { NextPage } from 'next'
import { useState } from 'react'
import { FaWeight } from 'react-icons/fa'
import 'react-toastify/dist/ReactToastify.css'
import { IUserData } from '../../../interface/global'
import useUsername from './Username.biz'

const UserName: NextPage<{ users: IUserData }> = ({ users }) => {
  useUsername(users)
  const [lenghtValue, setLenghtValue] = useState<number>()
  const [weightValue, setWeightValue] = useState<number>()
  const [showBmi, setShowBmi] = useState<boolean>(false)
  const bmi = (Number(weightValue) / Number(lenghtValue) ** 2) * 10000
  const calacBmi = () => {
    setShowBmi(true)
    return bmi
  }

  if (users.message) return null
  return (
    <>
      <div className="login_page_parrent column w-100 j-center a-center ">
        <h2>welcome {users.login}!</h2>
        <Box className="column login_box">
          <FaWeight style={{ marginBottom: '5px' }} size="2em" />
          <span>Calculate your BMI</span>
          <Input
            onChange={(e: any) => setLenghtValue(e.target.value)}
            value={lenghtValue}
            placeholder="Enter height"
            type="number"
          />

          <Input
            style={{ marginTop: '20px' }}
            placeholder="Enter weight"
            type="number"
            onChange={(e: any) => {
              setWeightValue(e.target.value), setShowBmi(false)
            }}
            value={weightValue}
          />
          <Button
            onClick={calacBmi}
            style={{ marginTop: '20px' }}
            colorScheme="blue"
            disabled={!lenghtValue || !weightValue}
          >
            Calculate
          </Button>
          <h2 style={{ marginTop: '4px' }}>{`BMI : ${showBmi ? bmi : ''}`}</h2>
        </Box>
      </div>

      {/* {bmiResult} */}
    </>
  )
}

export default UserName
