import { defineMessages } from 'react-intl'

const scope = 'loginMessage'
const loginMessage = defineMessages({
  login: {
    id: `${scope}.login`,
    defaultMessage: 'Login with your github username',
  },
  submitButton: {
    id: `${scope}.submitButton`,
    defaultMessage: 'Login',
  },
  fetchingProfile: {
    id: `${scope}.fetchingProfile`,
    defaultMessage: 'fetching bmi page',
  },
})
export default loginMessage
